all:
	echo "No default make target..."

terraform-init:
	docker run -ti -v $(shell pwd)/provisioning/digitalocean:/app/ -w /app/ hashicorp/terraform:latest init

terraform-plan:
	docker run -ti -v $(shell pwd)/provisioning/digitalocean:/app/ -w /app/ hashicorp/terraform:latest plan

terraform-apply:
	docker run -ti -v $(shell pwd)/provisioning/digitalocean:/app/ -w /app/ hashicorp/terraform:latest apply

terraform-destroy:
	docker run -ti -v $(shell pwd)/provisioning/digitalocean:/app/ -w /app/ hashicorp/terraform:latest destroy

ansible-k8s-setup:
	ansible-playbook install-kubernetes.yml

ansible-k8s-dashboard:
	ansible-playbook kubernetes-dashboard.yml
