# kubernetes-handson

## Terraform

* Generate a SSH key pair in `provisioning/ssh` called `ssh_key`:
    * `ssh-keygen -f provisioning/ssh/ssh_key`

* Add a `secret.auto.tfvars` with the following format in the `provisioning/digitalocean` folder:

```toml
do_token = "<DigitalOcean_API_Token>"
do_ssh_key_fingerprint = <DigitalOcean_SSH_Key_Fingerprint>
```

### Usage from Docker container

* `docker pull hashicorp/terraform:latest`
* Initialization: `docker run -ti -v $(pwd):/app/ -w /app/ hashicorp/terraform:latest init`
* Preparation: `docker run -ti -v $(pwd):/app/ -w /app/ hashicorp/terraform:latest plan`
* Provisoning: `docker run -ti -v $(pwd):/app/ -w /app/ hashicorp/terraform:latest apply`
* Tear down: `docker run -ti -v $(pwd):/app/ -w /app/ hashicorp/terraform:latest destroy`

## Ansible

### Inventory

* Python is broken on my machine so dynamic inventory parsing does not work (Aug 2018)
* Update IPs for new DigitalOcean droplet's IPs in `inventory/digitalocean/hosts`

### First run

* You will have to enter `yes` for each node upon first SSH connection
* If this is done during a playbook run, you should be able to do it anyways
