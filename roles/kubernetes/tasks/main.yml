---
- name: Install Kubernetes packages
  yum:
    name: "{{ item }}"
    state: present
  with_items:
    - kubeadm
    - kubelet
    - kubectl
    - kubernetes-cni
  loop_control:
    label: "Installing {{ item }}"
  notify: restart kubelet

- name: Start and enable Kubelet systemd service
  service:
    name: kubelet
    state: started
    enabled: yes

- name: Set Kubernetes cgroup-driver to "cgroupfs"
  replace:
    path: /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
    regexp: cgroup-driver=systemd
    replace: cgroup-driver=cgroupfs
  notify: restart kubelet

- name: Check if Kubernetes has already been initialized (for "kubeadm init" idempotency)
  stat:
    path: /etc/kubernetes/admin.conf
  register: kubernetes_init_stat

- name: Configure Kubernetes master nodes
  include_tasks: configure-masters.yml
  when: inventory_hostname in groups.k8s_master

- name: Get "kubeadm join" command from the Kubernetes master
  shell: kubeadm token create --print-join-command
  changed_when: False
  when: inventory_hostname in groups.k8s_master
  run_once: True
  register: kubernetes_join_command

- name: Configure Kubernetes worker nodes
  include_tasks: configure-workers.yml
  when: inventory_hostname in groups.k8s_worker
