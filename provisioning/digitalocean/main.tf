resource "digitalocean_tag" "k8s_master" {
    name = "k8s_master"
}


resource "digitalocean_droplet" "k8s_master" {
  count              = "${var.k8s_master_count}"

  image              = "${var.do_image}"
  region             = "${var.do_region}"
  size               = "${var.do_droplet_size}"
  name               = "k8s-master-${count.index + 1}"

  private_networking = false
  backups            = false
  ipv6               = false

  tags = ["${digitalocean_tag.k8s_master.name}"]
  ssh_keys = ["${var.do_ssh_key_fingerprint}"]
}

resource "digitalocean_tag" "k8s_worker" {
    name = "k8s_worker"
}

resource "digitalocean_droplet" "k8s_worker" {
  count              = "${var.k8s_worker_count}"

  image              = "${var.do_image}"
  region             = "${var.do_region}"
  size               = "${var.do_droplet_size}"
  name               = "k8s-worker-${count.index + 1}"

  private_networking = false
  backups            = false
  ipv6               = false

  tags = ["${digitalocean_tag.k8s_worker.name}"]
  ssh_keys = ["${var.do_ssh_key_fingerprint}"]
}
