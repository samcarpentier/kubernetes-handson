variable "do_token" {
  description = "DigitalOcean API token"
}

provider "digitalocean" {
  token = "${var.do_token}"
}
