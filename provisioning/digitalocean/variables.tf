variable "do_ssh_key_fingerprint" {
  description = "DigitalOcean SSH key fingerprint"
}

variable "do_region" {
  description = "DigitalOcean droplets region"
  default = "tor1"
}

variable "do_image" {
  description = "DigitalOcean droplets OS image slug"
  default = "centos-7-x64"
}

variable "do_droplet_size" {
  description = "DigitalOcean droplets size slug"
  default = "s-1vcpu-2gb"
}

variable "do_public_ssh_key" {
  description = "DigitalOcean SSH key to access droplets"
  default = "../ssh/ssh_key.pub"
}


variable "k8s_master_count" {
  description = "Number of master nodes in the Kubernetes cluster"
  default = "1"
}

variable "k8s_worker_count" {
  description = "Number of worker nodes in the Kubernetes cluster"
  default = "2"
}
